package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	// "strconv"
	"sync"
	"time"

	// "github.com/gorilla/mux"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
)

//endpoint to create a new party
func CreateParty(w http.ResponseWriter, r *http.Request) {
	party, err := payloadToParty(r)
	if err != nil {
		log.Println("error maping request to party", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	err = r.Body.Close()
	if err != nil {
		log.Println("error handling request", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	if party.Name == "" || party.DM == 0 {
		respondWithError(w, http.StatusBadRequest, "invalid party")
		return
	}

	var wg sync.WaitGroup
	validUsers, invalidUsers := make([]model.User, 0), make([]int, 0)

	wg.Add(1)

	dm := party.DM
	go func() {
		defer wg.Done()
		req, err := http.NewRequest("GET", fmt.Sprintf("http://sandbox-user-service/users/%d", dm), nil)
		if err != nil {
			log.Println("error verifying dungeon master", err.Error())
			invalidUsers = append(invalidUsers, dm)
			return
		}
		res, err := httpClient.Do(req)
		if err != nil {
			log.Println("error verifying dungeon master", err.Error())
			invalidUsers = append(invalidUsers, dm)
			return
		} else {
			if res.StatusCode == http.StatusOK {
				var dungeonMaster model.User
				err := json.NewDecoder(res.Body).Decode(&dungeonMaster)
				if err != nil {
					log.Println("error mapping dungeon master", err.Error())
					invalidUsers = append(invalidUsers, dm)
					return
				} else {
					validUsers = append(validUsers, dungeonMaster)
					return
				}
			} else {
				log.Println("error finding dungeon master")
				invalidUsers = append(invalidUsers, dm)
				return
			}
		}
	}()

	for _, userId := range party.Members {
		wg.Add(1)
		id := userId

		go func() {
			defer wg.Done()
			req, err := http.NewRequest("GET", fmt.Sprintf("http://sandbox-user-service/users/%d", id), nil)
			if err != nil {
				log.Println("error verifying member", err.Error())
				invalidUsers = append(invalidUsers, id)
				return
			}
			res, err := httpClient.Do(req)
			if err != nil {
				log.Println("error verifying member", err.Error())
				invalidUsers = append(invalidUsers, id)
				return
			} else {
				if res.StatusCode == http.StatusOK {
					var member model.User
					err := json.NewDecoder(res.Body).Decode(&member)
					if err != nil {
						log.Println("error mapping party member", err.Error())
						invalidUsers = append(invalidUsers, id)
						return
					} else {
						validUsers = append(validUsers, member)
						return
					}
				} else {
					log.Println("error finding party member")
					invalidUsers = append(invalidUsers, id)
					return
				}
			}
		}()
	}

	wg.Wait()

	if len(invalidUsers) > 0 {
		log.Println("invalid users detected", invalidUsers)
		respondWithError(w, http.StatusBadRequest, fmt.Sprintf("invalid users %+v", invalidUsers))
		return
	}

	party.CreatedDateTime = time.Now().UTC().Format(time.UnixDate)
	party.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

	err = db.QueryRow(
		"INSERT INTO dnd.party(name, created_date_time, updated_date_time, dm) VALUES($1, $2, $3, $4) RETURNING id",
		party.Name, party.CreatedDateTime, party.UpdatedDateTime, party.DM).Scan(&party.Id)

	if err != nil {
		log.Println("could not create party", err.Error())
		respondWithError(w, http.StatusInternalServerError, "internal service error")
		return
	}

	for _, user := range validUsers {
		_ = db.QueryRow("INSERT INTO sandbox.user_parties(user_id, party_id) VALUES ($1, $2)", user.Id, party.Id)
	}

	log.Println("created party", party.Id)

	respondWithJSON(w, http.StatusCreated, party)
}

// func GetItems(w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	userId := vars["userId"]

// 	//convert userId parameter to an int
// 	id, err := strconv.Atoi(userId)
// 	if err != nil {
// 		log.Println("could not convert user id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	//TODO: set in config
// 	countMin, countMax := 1, 100
// 	itemCount := 1
// 	c := r.URL.Query().Get("count")

// 	if c != "" {
// 		count, err := strconv.Atoi(c)
// 		if err != nil {
// 			log.Println("could not convert count to int", err.Error())
// 			respondWithError(w, http.StatusBadRequest, "invalid count")
// 			return
// 		}

// 		if count < countMin {
// 			log.Println("moving count from", count, "to", countMin)
// 			count = countMin
// 		} else if count > countMax {
// 			log.Println("moving count from", count, "to", countMax)
// 			count = countMax
// 		}

// 		itemCount = count
// 	}

// 	items := make([]model.Item, 0)
// 	rows, err := db.Query("SELECT * FROM dnd.item WHERE user_id = $1 LIMIT $2", id, itemCount)
// 	if err != nil {
// 		if err.Error() == "sql: no rows in result set" {
// 			respondWithJSON(w, http.StatusOK, items)
// 			return
// 		} else {
// 			respondWithError(w, http.StatusInternalServerError, "internal service error")
// 			return
// 		}
// 	}

// 	for rows.Next() {
// 		var newItem model.Item
// 		if err := rows.Scan(&newItem.Id, &newItem.Owner, &newItem.Name, &newItem.Description, &newItem.CreatedDateTime, &newItem.UpdatedDateTime); err != nil {
// 			log.Println("error reading row", err.Error())
// 			continue
// 		}

// 		items = append(items, newItem)
// 	}

// 	err = rows.Close()
// 	if err != nil {
// 		log.Println("error closing rows", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	respondWithJSON(w, http.StatusOK, items)
// 	return
// }

// func GetItem(w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	userId, itemId := vars["userId"], vars["itemId"]

// 	//convert userId parameter to an int
// 	_userId, err := strconv.Atoi(userId)
// 	if err != nil {
// 		log.Println("could not convert user id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	_itemId, err := strconv.Atoi(itemId)
// 	if err != nil {
// 		log.Println("could not convert item id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	item := model.Item{}
// 	err = db.QueryRow("SELECT * FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId).Scan(
// 		&item.Id, &item.Owner, &item.Name, &item.Description, &item.CreatedDateTime, &item.UpdatedDateTime)
// 	if err != nil {
// 		log.Println("error querying item", err.Error())
// 		if err.Error() == "sql: no rows in result set" {
// 			respondWithError(w, http.StatusNotFound, "item not found")
// 			return
// 		} else {
// 			respondWithError(w, http.StatusInternalServerError, "internal service error")
// 			return
// 		}
// 	}

// 	respondWithJSON(w, http.StatusOK, item)
// 	return
// }

// // For updating the Name or Description of a User's Item
// func UpdateItem(w http.ResponseWriter, r *http.Request) {
// 	item, err := payloadToItem(r)
// 	if err != nil {
// 		log.Println("error maping request to item", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	err = r.Body.Close()
// 	if err != nil {
// 		log.Println("error handling request", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	vars := mux.Vars(r)
// 	userId, itemId := vars["userId"], vars["itemId"]

// 	//convert userId parameter to an int
// 	_userId, err := strconv.Atoi(userId)
// 	if err != nil {
// 		log.Println("could not convert user id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	_itemId, err := strconv.Atoi(itemId)
// 	if err != nil {
// 		log.Println("could not convert item id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	if item.Name == "" || _itemId != item.Id {
// 		respondWithError(w, http.StatusBadRequest, "invalid changes")
// 		return
// 	}

// 	if _userId != item.Owner {
// 		log.Println("unauthorized request. user ids do not match", _userId, item.Owner)
// 		respondWithError(w, http.StatusUnauthorized, "unauthorized")
// 		return
// 	}

// 	_item := model.Item{}
// 	err = db.QueryRow("SELECT * FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId).Scan(
// 		&_item.Id, &_item.Owner, &_item.Name, &_item.Description, &_item.CreatedDateTime, &_item.UpdatedDateTime)
// 	if err != nil {
// 		log.Println("error querying item", err.Error())
// 		if err.Error() == "sql: no rows in result set" {
// 			respondWithError(w, http.StatusNotFound, "item not found")
// 			return
// 		} else {
// 			respondWithError(w, http.StatusInternalServerError, "internal service error")
// 			return
// 		}
// 	}

// 	item.Id = _item.Id
// 	item.Owner = _item.Owner
// 	item.CreatedDateTime = _item.CreatedDateTime
// 	item.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

// 	_, err = db.Exec("UPDATE dnd.item SET name = $1, description = $2, updated_date_time = $3 WHERE id = $4 AND user_id = $5",
// 		item.Name, item.Description, item.CreatedDateTime, item.Id, item.Owner)
// 	if err != nil {
// 		log.Println("error querying item", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	respondWithJSON(w, http.StatusOK, item)
// 	return
// }

// // For changing the Ownership of an Item from one User to another
// func ResetItem(w http.ResponseWriter, r *http.Request) {
// 	item, err := payloadToItem(r)
// 	if err != nil {
// 		log.Println("error maping request to item", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	err = r.Body.Close()
// 	if err != nil {
// 		log.Println("error handling request", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	vars := mux.Vars(r)
// 	userId, itemId := vars["userId"], vars["itemId"]

// 	//convert userId parameter to an int
// 	_userId, err := strconv.Atoi(userId)
// 	if err != nil {
// 		log.Println("could not convert user id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	_itemId, err := strconv.Atoi(itemId)
// 	if err != nil {
// 		log.Println("could not convert item id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	if item.Name == "" || _itemId != item.Id {
// 		respondWithError(w, http.StatusBadRequest, "invalid changes")
// 		return
// 	}

// 	if _userId == item.Owner && 0 != item.Owner {
// 		log.Println("unauthorized request. user ids do not match", _userId, item.Owner)
// 		respondWithError(w, http.StatusUnauthorized, "unauthorized")
// 		return
// 	}

// 	item.CreatedDateTime = time.Now().UTC().Format(time.UnixDate)
// 	item.UpdatedDateTime = time.Now().UTC().Format(time.UnixDate)

// 	err = db.QueryRow(
// 		"INSERT INTO dnd.item(user_id, name, description, created_date_time, updated_date_time) VALUES($1, $2, $3, $4, $5) RETURNING id",
// 		item.Owner, item.Name, item.Description, item.CreatedDateTime, item.UpdatedDateTime).Scan(&item.Id)

// 	if err != nil {
// 		log.Println("could not create item", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	log.Println("reset item", item.Id)

// 	_, err = db.Exec("DELETE FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId)
// 	if err != nil {
// 		log.Println("error querying item", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	log.Println("delted _item", _itemId)

// 	respondWithJSON(w, http.StatusOK, item)
// 	return
// }

// func DeleteItem(w http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)
// 	userId, itemId := vars["userId"], vars["itemId"]

// 	//convert userId parameter to an int
// 	_userId, err := strconv.Atoi(userId)
// 	if err != nil {
// 		log.Println("could not convert user id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	_itemId, err := strconv.Atoi(itemId)
// 	if err != nil {
// 		log.Println("could not convert item id to int", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	if _userId < 0 {
// 		log.Println("invalid user", _userId)
// 		respondWithError(w, http.StatusUnauthorized, "unauthorized")
// 		return
// 	}

// 	if _itemId < 0 {
// 		log.Println("invalid item", _itemId)
// 		respondWithError(w, http.StatusBadRequest, "bad request")
// 		return
// 	}

// 	_, err = db.Exec("DELETE FROM dnd.item WHERE id = $1 AND user_id = $2", _itemId, _userId)
// 	if err != nil {
// 		log.Println("error querying item", err.Error())
// 		respondWithError(w, http.StatusInternalServerError, "internal service error")
// 		return
// 	}

// 	log.Println("delted _item", _itemId)

// 	respondWithJSON(w, http.StatusNoContent, nil)
// 	return
// }

//Converts HTTP request body into a model.User struct
func payloadToUser(r *http.Request) (model.User, error) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Println("error decoding user", err.Error())
	}
	return user, err
}

//Converts HTTP request body into a model.Party struct
func payloadToParty(r *http.Request) (model.Party, error) {
	var party model.Party
	err := json.NewDecoder(r.Body).Decode(&party)
	if err != nil {
		log.Println("error decoding party", err.Error())
	}
	return party, err
}

//Clears signed string token and sets HTTP Response body to map of error messages
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

//Sets status, body, and default headers to HTTP Response
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}
