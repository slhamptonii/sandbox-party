FROM golang:alpine

ARG env
ARG port

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

LABEL maintainer="Sheldon L Hampton II <tharivolnailo@protonmail.com>"

WORKDIR /app

COPY go.mod go.sum handlers.go main.go ./
COPY env/ ./env/

# Download all dependancies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

RUN go build -o main .

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl --HEAD http://localhost/health || exit 1

EXPOSE $port

ENV ENVIRONMENT=$env
ENV PORT=$port

# Run the executable
CMD ./main --env=$ENVIRONMENT --port=$PORT
