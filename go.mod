module gitlab.com/slhamptonii/sandbox-party

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.9.0
	gitlab.com/slhamptonii/sheldonsandbox-core/v3 v3.5.5
	gopkg.in/yaml.v2 v2.4.0
)
